import React, { useEffect, useState } from "react";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import "assets/css/MiniCalendar.css";
import { Text, Icon, useColorModeValue } from "@chakra-ui/react";
// Chakra imports
import { MdChevronLeft, MdChevronRight } from "react-icons/md";
// Custom components
import Card from "components/card/Card.js";

export default function MiniCalendar(props) {
  const { selectRange, new_value, action, ...rest } = props;
  const [value, onChange] = useState(new Date(null));
  // Chakra Color Mode
  const borderColor = useColorModeValue("transparent", "whiteAlpha.100");

  const changeHandler = (val) => {
    console.log("DATE: ", val);
    onChange(val);
    props.action(val);
  };

  useEffect(() => {
    if (new_value) onChange(new Date(new_value));
  }, [new_value]);
  return (
    <Card
      border="1px solid"
      borderColor={borderColor}
      align="center"
      direction="column"
      w="100%"
      maxW="max-content"
      p="20px 15px"
      h="max-content"
      {...rest}
    >
      <Calendar
        onChange={(e) => changeHandler(e)}
        value={value}
        selectRange={selectRange}
        view={"month"}
        border={"0px solid white"}
        tileContent={<Text color="brand.500"></Text>}
        prevLabel={<Icon as={MdChevronLeft} w="24px" h="24px" mt="4px" />}
        nextLabel={<Icon as={MdChevronRight} w="24px" h="24px" mt="4px" />}
      />
    </Card>
  );
}
