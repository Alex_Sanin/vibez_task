/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   ____  ____   ___  
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| |  _ \|  _ \ / _ \ 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || |  | |_) | |_) | | | |
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |  |  __/|  _ <| |_| |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___| |_|   |_| \_\\___/ 
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI Dashboard PRO - v1.0.0
=========================================================

* Order Page: https://www.horizon-ui.com/pro/
* Copyright 2022 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React, { useState, useEffect } from "react";

// Chakra imports
import { Flex, Text } from "@chakra-ui/react";

// Custom components
import { columnsData } from "./components/ColumnsData";

// Assets
import { ItemsView } from "./components/ItemsView";
import { Spinner } from "components/spinner";

export default function EventItems({ event_path, user }) {
  const [items, setItems] = useState([]);
  const [error, setError] = useState(null);
  const [inProgress, setInProgress] = useState(false);

  const fetchOrderItems = async () => {
    if (inProgress) return;
    if (!user) return;
    setInProgress(true);
    const response = await fetch(event_path + "/items", {
      method: "GET",
      headers: {
        "Access-Control-Allow-Credentials": true,
        "Content-Type": "application/json",
        Authorization: user,
      },
    });
    const json = await response.json();
    console.log("GET ITEMS RES: ", json);

    if (response.ok) {
      let scope = json.items.filter(
        (i) => i?.order?.state == "approved" && i?.status != "canceled"
      );
      setItems(scope);
    } else {
      setError(response?.error?.message);
    }
    setInProgress(false);
  };

  useEffect(() => {
    localStorage.setItem("event_route", "items");
    if (user && event_path) {
      fetchOrderItems();
    }
  }, [user]);

  // Chakra Color Mode
  return (
    <Flex
      w="100%"
      justifyContent={"center"}
      direction={"column"}
      alignItems={"center"}
    >
      {error && <Text>{error}</Text>}
      {inProgress && <Spinner />}
      {items && items.length > 0 && (
        <ItemsView
          user={user}
          event_path={event_path}
          setError={setError}
          items={items}
          columnsData={columnsData}
          fetchOrderItems={fetchOrderItems}
          tableData={items}
        />
      )}
    </Flex>
  );
}
