import React from "react";
import ReactDOM from "react-dom";
import "assets/css/App.css";
import "mapbox-gl/dist/mapbox-gl.css";
import { HashRouter, Route, Switch, Redirect } from "react-router-dom";
import { AuthContextProvider } from "contexts/AuthContext";

// Chakra imports
import { ChakraProvider } from "@chakra-ui/react";
import theme from "theme/theme";

//custom imports
import SignIn from "views/auth/signIn/SignInDefault";
import SignUp from "views/auth/signUp/SignUpDefault";
import ForgotPassword from "views/auth/forgotPassword/ForgotPasswordDefault";
import ResetPassword from "views/auth/resetPassword/resetPassword";
import EmailConfirmation from "views/auth/emailConfirmation/emailConfirmation";
import OthersError from "views/admin/main/others/404";
import Licenses from "views/auth/licenses";
import Project from "views/admin/producer/project";
import ProjectList from "views/admin/producer/projects";
import Event from "views/admin/producer/event";
import { ProjectContextProvider } from "./contexts/ProjectContext";
import HttpsRedirect from "react-https-redirect";

ReactDOM.render(
  <ChakraProvider theme={theme}>
    <HttpsRedirect>
      <AuthContextProvider>
        <ProjectContextProvider>
          <React.StrictMode>
            <HashRouter>
              <Switch>
                {/* Auth */}
                <Route path={`/auth/sign-in`} component={SignIn} />
                <Route path={`/auth/sign-up`} component={SignUp} />
                <Route
                  path={`/auth/forgot-password`}
                  component={ForgotPassword}
                />
                <Route
                  path={`/auth/confirm-email`}
                  component={EmailConfirmation}
                />
                <Route
                  path={`/auth/reset-password`}
                  component={ResetPassword}
                />
                <Route path={`/auth/licenses`} component={Licenses} />

                {/*Producer's view*/}
                <Route path={`/projects/:id/events/:id`} component={Event} />
                <Route path={`/projects/:id`} component={Project} />
                <Route path={`/projects`} component={ProjectList} />
                {/* Other */}
                <Redirect from="/" to="/auth/sign-in" component={SignIn} />
                <Route from="*" to="/404" component={OthersError} />
              </Switch>
            </HashRouter>
          </React.StrictMode>
        </ProjectContextProvider>
      </AuthContextProvider>
    </HttpsRedirect>
  </ChakraProvider>,
  document.getElementById("root")
);
